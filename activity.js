// input
db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);



// ========================= 1 
Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
  {
    $match: { onSale: true }
  },
  {
    $count: "fruitsOnSale"
  }
]);


// ========================= 2
Use the count operator to count the total number of fruits with stock more than or equal to 20.

db.fruits.aggregate([
    {
      $match: {
        stock: {$gte: 20}
      }
    },
    {
      $count: "enoughStock"
    }
]);


// ========================= 3 
Use the average operator to get the average price of fruits on sale per supplier.


db.fruits.aggregate([
    {
      $match: {onSale: true}
    },
    {
      $group: {
        _id: "$supplier_id",
        avg_price: {$avg: "price"}
      }
    }
]);


// ========================= 4
Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
    {
      $match: {onSale: true}
    },
    {
      $group: {
        _id: "$supplier_id",
        max_price: {$max: "$price"}
      }
    }
]);


// ========================= 5
Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
    {
      $match: {onSale: true}
    },
    {
      $group: {
        _id: "$supplier_id",
        min_price: {$min: "$price"}
      }
    }
]);














// Single-Purpose Aggregation - only has one stage and is mainly use for a specific purpose 
db.course_bookings.count();

// Aggregation can have one or more stages. In this case we are only using the $group stage to output specific fields that we need
db.course_bookings.aggregate([
    {
      $group: { _id: null, count: {$sum: 1}}
    }
]);



db.course_bookings.aggregate([
    {
      $match: {
        isCompleted: true
      }
    },
    {
      $group: {
        _id: "$courseId",
        total: {$sum: 1}
      }
    }
]);
